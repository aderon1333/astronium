﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class die : MonoBehaviour {
	public int health;
	private int currentHealth;
	public GameObject blood1,blood2;
	void Start () {
		currentHealth = health;
	}
	
	public void TakeDamage(){
		health--;
		Instantiate(blood2,transform.position,Quaternion.identity);
		if(health<=0){
			Die();
		}
	}
	
	public void Die(){
		Instantiate(blood1,transform.position,Quaternion.identity);
		Destroy(gameObject);
	}
}
