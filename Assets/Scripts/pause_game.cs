﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class pause_game : MonoBehaviour {

	public Transform canvas;
	public Button mybutton;
	public Sprite paused;
	public Sprite unpaused;
	
	// Update is called once per frame
	void Start() {
		mybutton = GetComponent<Button>();
	}

	public void Pause() {
		if (Time.timeScale == 1) {
			mybutton.image.overrideSprite = unpaused;
		}
		else {
			mybutton.image.overrideSprite = paused;
		}
		if (canvas.gameObject.activeInHierarchy == false) {
			canvas.gameObject.SetActive(true);
			Time.timeScale = 0;
			mybutton.image.overrideSprite = paused;
		}
		else {
			canvas.gameObject.SetActive(false);
			Time.timeScale = 1;
			mybutton.image.overrideSprite = unpaused;
		} 
	}
	}


