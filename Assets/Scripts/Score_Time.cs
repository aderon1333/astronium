﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score_Time : MonoBehaviour {
	public Text scoreText;
	public Text highscoreText;
	public float speed;
	public float score,bestScore;
	// Use this for initialization
	void Start()
    {
		highscoreText.text = PlayerPrefs.GetFloat("HighScore", 0).ToString("0"); 
        score = 0;
		speed = 1;
    }
	
	// Update is called once per frame
	void Update () {
		score += (speed*Time.deltaTime);
		scoreText.text = score.ToString("0");
		if (score > PlayerPrefs.GetFloat("HighScore", 0)){
			PlayerPrefs.SetFloat("HighScore",score);
			highscoreText.text = score.ToString("0");
		}
		
	}
}
