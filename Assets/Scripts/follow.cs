﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class follow : MonoBehaviour {

	public Transform EndPoint;
	public float speed;
	// Use this for initialization
	void Start () {
		speed = 90f;
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = Vector3.MoveTowards(transform.position,EndPoint.position,speed*Time.deltaTime);
	}
}
