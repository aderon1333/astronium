﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class eye_shoot : MonoBehaviour {

	public float speed = 1000; 
	public Rigidbody2D bullet; 
	public Transform gunPoint; 
	public float fireRate = 5; 

	private float curTimeout;
	void Update()
	{
		Fire();
	}

	void Fire()
	{
		curTimeout += Time.deltaTime;
		if(curTimeout > fireRate)
		{
			curTimeout = 0;
			Rigidbody2D clone = Instantiate(bullet, gunPoint.position, Quaternion.identity) as Rigidbody2D;
			clone.velocity = transform.TransformDirection(-speed*gunPoint.up);
			clone.transform.up = gunPoint.up;
		}
	}

}
