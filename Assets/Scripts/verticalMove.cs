﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class verticalMove : MonoBehaviour {

	public float speed;

	// Use this for initialization
	void Start () {
		speed = Random.Range(150f,200);
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate(0,-speed * Time.deltaTime,0);
	}
}
