﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Advertisements;

public class w : MonoBehaviour {
	
	public Text AdCheck;
	public int AdCheckINT;
	public Transform canvas;
	public float speed = 200f;
	private Rigidbody2D rb;
	void Start () {
		rb = GetComponent <Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
		float moveX = Input.GetAxis ("Horizontal");
		rb.MovePosition(rb.position + Vector2.right * moveX * speed * Time.deltaTime);
		int.TryParse(AdCheck.text, out AdCheckINT);
	}

	void OnTriggerEnter2D(Collider2D other ){
		if (other.gameObject.tag == "Enemy" || other.gameObject.tag == "Bullet"){
			if((AdCheckINT % 4)==0){
				if (Advertisement.IsReady("video")) {
					Advertisement.Show("video");
				}
			}	
			if (canvas.gameObject.activeInHierarchy == false) {
					canvas.gameObject.SetActive(true);
					Time.timeScale = 0;
			}
		}
	}

	void ReloadLevel(){
		canvas.gameObject.SetActive(false);
		Time.timeScale = 1;
		SceneManager.LoadScene("e");
	}	
}	

