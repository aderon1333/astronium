﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
[RequireComponent(typeof(Rigidbody2D))]


public class eye_bulletShoot : MonoBehaviour {
	void OnTriggerEnter2D(Collider2D other)
	{
			if(other.gameObject.tag == "Player"){
					Destroy(gameObject);
			}
	}

	void OnBecameInvisible(){
		Destroy(gameObject);
	}
	
}
