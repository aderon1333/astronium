using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shoot : MonoBehaviour {

	Rigidbody2D myRigidBody;
	public float moveSpeed;

	// Use this for initialization
	void Start () {
		myRigidBody = GetComponent<Rigidbody2D>();
		myRigidBody.AddRelativeForce(Vector2.up*moveSpeed,ForceMode2D.Impulse);
	}
	
	void OnBecameInvisible(){
		Destroy(gameObject);
	}
	void OnTriggerEnter2D(Collider2D other){
		if(other.tag == "Enemy"){
			if(other.GetComponent<die>() != null){
				other.GetComponent<die>().TakeDamage();
				Destroy(gameObject);
			}
		}
	}
}
