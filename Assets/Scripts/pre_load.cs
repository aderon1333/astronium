﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]

public class pre_load : MonoBehaviour {
	public Transform canvas;

	// Use this for initialization
	void Start () {
		canvas.gameObject.SetActive(false);
		Time.timeScale = 1;
	}

	void Awake() {
		Time.timeScale = 1;
	}
}
