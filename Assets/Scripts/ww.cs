﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Advertisements;
using UnityEngine.SceneManagement;

public class ww : MonoBehaviour {
	public Text AdCheck;
	public int AdCheckINT;
	public Transform canvas;
	public Transform canvasSecond;


	void OnTriggerEnter2D(Collider2D other ){
		if (other.gameObject.tag == "Enemy"){
			if (canvas.gameObject.activeInHierarchy == false) {
				if((AdCheckINT % 4)==0){
					if (Advertisement.IsReady("video")) {
						Advertisement.Show("video");
					}
				}
					canvas.gameObject.SetActive(true);
					Time.timeScale = 0;
				}
		}
	}

	void ReloadLevel(){
		canvas.gameObject.SetActive(false);
		Time.timeScale = 1;
		SceneManager.LoadScene("e");
	}

	void Update() {
		if (canvas.gameObject.activeInHierarchy == true) {
			canvasSecond.gameObject.SetActive(false);
		}
		int.TryParse(AdCheck.text, out AdCheckINT);
	}
}
