﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class circ : MonoBehaviour {

	public float speed;

	// Use this for initialization
	void Start () {
		speed = Random.Range(100f,150);
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate(speed * Time.deltaTime,0,0);
		if (transform.position.x > -40){
			speed = -speed;
		}
		if(transform.position.x < 800){
			speed = Random.Range(100f,150);
		}
	}
}
