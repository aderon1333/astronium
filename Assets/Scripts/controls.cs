using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controls : MonoBehaviour {
		private float xMin,xMax,yMin,yMax;
		public float speed = 900f;
		private Rigidbody2D rb;
		public float leftBorder;
    	public float rightBorder;

		void Start () {
			leftBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0)).x;
       		rightBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0)).x;
			rb = GetComponent <Rigidbody2D>();
			yMin=0;
			yMax=200;
		}

		void Update() {
			if (Input.touchCount == 1) {
				foreach (Touch touch in Input.touches) {
						GetComponent<Rigidbody2D>().position = new Vector2
				(
					Mathf.Clamp(GetComponent<Rigidbody2D>().position.x,leftBorder+85,rightBorder-85),
					Mathf.Clamp(GetComponent<Rigidbody2D>().position.y,yMin,yMax)
				); 

				if (touch.position.x  < Screen.width/2) {
					rb.MovePosition(rb.position + Vector2.left * speed * Time.deltaTime);
				}

				if (touch.position.x  > Screen.width/2) {
					rb.MovePosition(rb.position + Vector2.right * speed * Time.deltaTime);
				}
				}
			}
		}

	
}	