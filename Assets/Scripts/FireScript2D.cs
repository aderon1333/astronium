﻿using UnityEngine;
using System.Collections;

public class FireScript2D : MonoBehaviour {

	public float speed = 12;
	public Rigidbody2D bullet; 
	public Transform gunPoint;
	public float fireRate = 3;

	private float curTimeout;
	void Update()
	{
		Fire();
	}

	void Fire()
	{
		curTimeout += Time.deltaTime;
		if(curTimeout > fireRate)
		{
			curTimeout = 0;
			Rigidbody2D clone = Instantiate(bullet, gunPoint.position, Quaternion.identity) as Rigidbody2D;
			clone.velocity = transform.TransformDirection(gunPoint.up * speed);
			clone.transform.up = gunPoint.up;
		}
	}
}