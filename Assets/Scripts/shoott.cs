﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shoott : MonoBehaviour {

	public GameObject currentProjectile;
	public float shootDelay;

	private float shootDelayCounter;

	// Use this for initialization
	void Start () {
		shootDelayCounter = 0;
	}
	
	// Update is called once per frame
	void Update () {
		Shoot();
	}

	private void Shoot(){
		if (shootDelayCounter <= 0){
			Instantiate(currentProjectile,transform.position,transform.rotation);
			shootDelayCounter = shootDelay;
		}
		shootDelayCounter -= Time.deltaTime;
	}
}
