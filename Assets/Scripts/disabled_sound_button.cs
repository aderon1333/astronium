﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[RequireComponent(typeof(Button))]
public class disabled_sound_button : MonoBehaviour {
	public Button mybutton;
	public Sprite sound_1;
	public Sprite sound_1_disabled;
	private int counter = 0;
	// Use this for initialization
	void Start () {
		AudioSource IntroAction = GetComponent<AudioSource> ();
		mybutton = GetComponent<Button>();
	}
	
	// Update is called once per frame
	public void changeButton () {
		counter++;
		if (counter % 2 == 0) {
			mybutton.image.overrideSprite = sound_1;
			AudioListener.volume = 1.0F;
		}
		else {
			mybutton.image.overrideSprite = sound_1_disabled;
			AudioListener.volume = 0.0F;
		}
	}
}
