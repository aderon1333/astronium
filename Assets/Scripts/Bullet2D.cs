﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]

public class Bullet2D : MonoBehaviour {
	
	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.tag == "Enemy"){
			if(other.GetComponent<die>() != null){
				other.GetComponent<die>().TakeDamage();
				Destroy(gameObject);
			}
		}
	}

	void OnBecameInvisible(){
		Destroy(gameObject);}
}

	


