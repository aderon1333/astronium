﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemySpawn : MonoBehaviour {

	public Text other;
	public int other_INT;
	public GameObject obj;
	public GameObject bigobj;
	public GameObject eye;
	
	
	// Update is called once per frame
	void Start () {
		StartCoroutine("instobj");
		StartCoroutine("instbigobj");
		StartCoroutine("insteye");
	}

	IEnumerator instobj(){
		while (true){
			Instantiate (obj, new Vector3(Random.Range(20f,750),Random.Range(1300f,1500),Random.Range(-20f,-100)),Quaternion.identity);
			yield return new WaitForSeconds (3f);
		}
	}

	IEnumerator instbigobj(){
		yield return new WaitUntil(() => other_INT == Random.Range(10,15));
		while (true){
			yield return new WaitForSeconds (4f);
			Instantiate (bigobj, new Vector3(Random.Range(50f,800),Random.Range(1450f,1500),Random.Range(-20f,-100)),Quaternion.identity);
			yield return new WaitForSeconds (10f);
		}
	}

	IEnumerator insteye(){
		yield return new WaitUntil(() => other_INT == Random.Range(25,30));
		while (true){
			yield return new WaitForSeconds (3f);
			Instantiate (eye, new Vector3(Random.Range(150f,700f),Random.Range(1400f,1500),-10f),Quaternion.identity);
			yield return new WaitForSeconds (4f);
		}
	}

	public void Update() {
		int.TryParse(other.text, out other_INT);
	}
}
