﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class key_controls : MonoBehaviour {

	public float speed = 500f; 
	private float xMin,xMax;
	private Rigidbody2D rb; 
	void Start () { 
		rb = GetComponent <Rigidbody2D>(); 
		
	} 

	void Update () { 

		float moveX = Input.GetAxis ("Horizontal"); 
		rb.MovePosition(rb.position + Vector2.right * moveX * 600 * Time.deltaTime); 
			GetComponent<Rigidbody2D>().position = new Vector2
			(
				Mathf.Clamp(GetComponent<Rigidbody2D>().position.x,xMin,xMax),
				0.0f
			); 

	}

	void OnTriggerEnter2D(Collider2D other ){
		if (other.gameObject.tag == "Enemy"){
			ReloadLevel();
		}
	}

	void ReloadLevel(){
		SceneManager.LoadScene("e");
	}
}
