﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class circDirection : MonoBehaviour {

	public float speed;
	public float leftBorder;
    public float rightBorder;

	// Use this for initialization
	void Start () {
		Camera camera = GetComponent<Camera>();
		speed = Random.Range(150f,200);
		leftBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0)).x;
        rightBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0)).x;
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate(speed * Time.deltaTime,0,0);
		if (transform.position.x > rightBorder-70){
			speed = -speed;
		}
		if(transform.position.x < leftBorder+70){
			speed = Random.Range(150f,200);
		}
	}
}
